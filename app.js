var express= require('express');
var bodyParser = require('body-parser');
var path = require('path');
var mongo = require('mongodb').MongoClient;
var assert = require('assert');
var app = express();
var router = require('./routes/index');

var url = "mongodb://localhost:27017/testm"

//view engine
app.set('view engine','ejs');
app.set('views',path.join(__dirname,'views'));

//body-parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//set static path
app.use(express.static(path.join(__dirname,'public')));

app.use('/',router);

app.listen(3000,function(){
    console.log("server started at http://localhost:3000");  
});



