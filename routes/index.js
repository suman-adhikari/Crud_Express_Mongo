var express= require('express');
var router = express.Router();
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');

var url= "mongodb://localhost:27017/testm";


router.get('/',function(req,res,next){
  var itemArray = [];
  mongo.connect(url,function(err,db){
      assert.equal(null,err);
      var cursor = db.collection("std").find();
      cursor.forEach(function(doc,err){
          assert.equal(null,err);
          itemArray.push(doc);
      },function(){
          db.close();
          res.render("index",{
              title:"CRUD using mongo and express",
              item:itemArray
          })
      })
  });
});


router.post('/save',function(req,res,next){
    var item = {
        id:1,
        name:req.body.name,
        address:req.body.address
    }
    mongo.connect(url,function(err,db){
       assert.equal(null,err);
       db.collection('std').insertOne(item,function(){
           assert.equal(null,err);
           db.close();
           console.log("item inserte");
           res.redirect('/');
       }) 
    });
})

router.post('/update',function(req,res,next){
    var item ={
        name:req.body.name,
        address:req.body.address
    };

    var id = req.body.id;
    mongo.connect(url,function(err,db){
        assert.equal(null,err);
        db.collection('std').updateOne({ "_id": objectId(id)}, {$set: item}, function(err,result){
            assert.equal(null,err);
            db.close();
            res.redirect('/');
        }) 
    });
});

router.post('/delete',function(req,res,next){
    var id = req.body.id;
    mongo.connect(url,function(err,db){
       assert.equal(null,err);
       db.collection('std').deleteOne({"_id":objectId(id)},function(err,result){
           assert.equal(null,err);
           db.close();
           res.redirect('/');
       })
    });
});

module.exports = router;